#!/usr/bin/env bash

IP=$(realpath "$(dirname "$0")")

cmd="date -u +%V\`uname\`|sha256sum|sed 's/\W//g'"
answer="$(eval "$cmd")"

declare -A prompts
prompts[en]='What is the output of the command <tt>%s</tt>?'

{
	echo '<?php'
	echo -n '$wgFunnyQuestions = array('

	nl='\n'
	for lang in "${!prompts[@]}"; do
		printf "${nl}\t\"${lang}\" => array(\"${prompts[$lang]}\" => \"%s\")" "$cmd" "$answer"
		nl=',\n'
	done

	echo
	echo ');'
} > "$IP/FunnyQuestion.conf.php"

touch "$IP/extensions/FunnyQuestion/FunnyQuestion.i18n.php"
